package fi.iki.asb.pogo.android;

import java.util.ArrayList;
import java.util.List;

import fi.iki.asb.pogo.calculator.PokemonCollector;
import fi.iki.asb.pogo.pokemon.Pokemon;

public class PokemonCollectorImpl implements PokemonCollector {

    private List<Pokemon> matches = new ArrayList<>(16);

    private PokemonCollectorListener listener = null;

    public void setListener(PokemonCollectorListener listener) {
        this.listener = listener;
    }

    @Override
    public void start() {
        matches.clear();
    }

    @Override
    public void add(Pokemon pokemon) throws InterruptedException {
        if (matches.size() >= 16) {
            throw new InterruptedException("Enough!");
        }

        matches.add(pokemon);
    }

    @Override
    public void end(boolean interrupted) {
        if (listener != null) {
            listener.onCollectingEnded(matches, interrupted);
        }
    }
}
