package fi.iki.asb.pogo.android;

import static fi.iki.asb.pogo.android.Const.*;

import java.util.Scanner;

import fi.iki.asb.pogo.pokemon.Appraisal;
import fi.iki.asb.pogo.pokemon.Lucky;
import fi.iki.asb.pogo.pokemon.Pokemon;
import fi.iki.asb.pogo.pokemon.PoweredUp;

public class PokemonSerializer {

    public static String write(Pokemon p) {
        StringBuilder sb = new StringBuilder();
        sb.append(p.getSpecies().getName()).append('\n');
        sb.append(p.getAppraisal().getPerfectionTier()).append('\n');
        sb.append(p.getAppraisal().getTopStatTier()).append('\n');
        sb.append(p.getAppraisal().isAttackBest()).append('\n');
        sb.append(p.getAppraisal().isDefenseBest()).append('\n');
        sb.append(p.getAppraisal().isStaminaBest()).append('\n');
        sb.append(p.getCp()).append('\n');
        sb.append(p.getHp()).append('\n');
        sb.append(p.getIndividualValues().getAttack()).append('\n');
        sb.append(p.getIndividualValues().getDefense()).append('\n');
        sb.append(p.getIndividualValues().getStamina()).append('\n');
        sb.append(p.getLevel().getName()).append('\n');
        sb.append(p.getPowerUpCost()).append('\n');
        sb.append(p.isLucky()).append('\n');
        sb.append(p.isPoweredUp()).append('\n');
        return sb.toString();
    }

    public static Pokemon fromString(String in) {
        final Scanner s = new Scanner(in);
        final Pokemon p = Pokemon.ofSpecies(DEX.findByName(s.nextLine()));
        p.getAppraisal().setPerfectionTier(Appraisal.PerfectionTier.fromLabel(nextString(s)));
        p.getAppraisal().setTopStatTier(Appraisal.TopStatTier.fromLabel(nextString(s)));
        p.getAppraisal().setAttackBest(nextBoolean(s));
        p.getAppraisal().setDefenseBest(nextBoolean(s));
        p.getAppraisal().setStaminaBest(nextBoolean(s));
        p.setCp(nextInteger(s));
        p.setHp(nextInteger(s));
        p.getIndividualValues().setAttack(nextInteger(s));
        p.getIndividualValues().setDefense(nextInteger(s));
        p.getIndividualValues().setStamina(nextInteger(s));
        p.setLevel(LEVELS.getLevel(nextString(s)));
        p.setPowerUpCost(nextInteger(s));
        p.setLucky(Lucky.fromLabel(nextString(s)));
        p.setPoweredUp(PoweredUp.fromLabel(nextString(s)));

        return p;
    }

    private static boolean nextBoolean(Scanner s) {
        String i = nextString(s);
        if (i == null)
            return false;
        else
            return Boolean.parseBoolean(i);
    }

    private static Integer nextInteger(Scanner s) {
        String i = nextString(s);
        if (i == null)
            return null;
        else
            return Integer.parseInt(i);
    }

    private static String nextString(Scanner s) {
        String i = s.nextLine();
        if ("null".equals(i))
            return null;
        else
            return i;
    }
}
