package fi.iki.asb.pogo.android;

import java.util.List;

import fi.iki.asb.pogo.pokemon.Pokemon;

public interface PokemonCollectorListener {

    /**
     * Collecting has ended.
     *
     * @param pokemon The found pokemon.
     * @param interrupted Was the collecting interrupted (true) or finished (false).
     */
    void onCollectingEnded(List<Pokemon> pokemon, boolean interrupted);

}
