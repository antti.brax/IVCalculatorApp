package fi.iki.asb.pogo.android;

import fi.iki.asb.pogo.calculator.Calculator;
import fi.iki.asb.pogo.calculator.Levels;
import fi.iki.asb.pogo.pokedex.Pokedex;

final class Const {

    static final Pokedex DEX = Pokedex.instance();

    static final Levels LEVELS = Levels.instance();

    static final Calculator CALC = Calculator.instance();

}
