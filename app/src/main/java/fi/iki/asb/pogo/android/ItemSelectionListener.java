package fi.iki.asb.pogo.android;

import android.view.View;
import android.widget.AdapterView;

public abstract class ItemSelectionListener implements AdapterView.OnItemSelectedListener {

    public abstract void onItemSelected(Object item);

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        final Object item = parent.getItemAtPosition(position);
        onItemSelected(item);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
