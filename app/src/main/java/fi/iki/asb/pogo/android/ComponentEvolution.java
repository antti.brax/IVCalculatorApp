package fi.iki.asb.pogo.android;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Locale;

import fi.iki.asb.pogo.pokemon.Pokemon;

public class ComponentEvolution extends LinearLayout {

    public ComponentEvolution(
            final Context context,
            final AttributeSet attrs,
            final Pokemon pokemon) {
        super(context, attrs);
        inflate(context, R.layout.component_evolution, this);

        final String label = String.format(Locale.ENGLISH,
                "%s (%d)",
                pokemon.getSpecies().getName(),
                pokemon.getCp());

        final TextView tv = findViewById(R.id.textViewContent);
        tv.setText(label);
        tv.setOnClickListener(new ChangePokemonOnClickListener(context, pokemon));
    }
}
