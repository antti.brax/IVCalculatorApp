package fi.iki.asb.pogo.android;

import java.util.ArrayList;
import java.util.List;

import fi.iki.asb.pogo.pokemon.Pokemon;

public class PokemonCollectorListenerGroup implements PokemonCollectorListener {

    private List<PokemonCollectorListener> listeners = new ArrayList<>();

    public PokemonCollectorListenerGroup add(PokemonCollectorListener listener) {
        listeners.add(listener);
        return this;
    }

    @Override
    public void onCollectingEnded(List<Pokemon> pokemon, boolean interrupted) {
        for (PokemonCollectorListener listener: listeners) {
            listener.onCollectingEnded(pokemon, interrupted);
        }
    }
}
