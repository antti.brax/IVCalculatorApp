package fi.iki.asb.pogo.android;

public interface Consumer<T> {

    void accept(T t);

}
