package fi.iki.asb.pogo.android;

import android.text.Editable;
import android.text.TextWatcher;

public class TextChangeNumberOverflowListener implements TextWatcher {

    private int maxValue = 9999;

    private int previousValue;

    private Consumer<Integer> onValueChanged = null;

    private Runnable onNextDigitOverflows = null;

    private Runnable onOverflow = null;

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        if (s.length() == 0) {
            previousValue = 0;
        } else {
            previousValue = Integer.valueOf(s.toString());
        }
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
        final String s = editable.toString();
        if (s.length() == 0) {
            if (onValueChanged != null) {
                onValueChanged.accept(null);
            }
            return;
        }

        final int currentValue = Integer.valueOf(s);
        if (currentValue != previousValue) {
            if (onValueChanged != null) {
                onValueChanged.accept(currentValue);
            }
        }

        // Value has overflown.
        if (currentValue > maxValue) {
            if (onOverflow != null) {
                onOverflow.run();
            }
            return;
        }

        final int nextValue = currentValue * 10;
        if (nextValue > maxValue && currentValue > previousValue) {
            if (onNextDigitOverflows != null) {
                onNextDigitOverflows.run();
            }
        }
    }

    public void setOnValueChanged(Consumer<Integer> onValueChanged) {
        this.onValueChanged = onValueChanged;
    }

    public void setOnNextDigitOverflows(Runnable onNextDigitOverflows) {
        this.onNextDigitOverflows = onNextDigitOverflows;
    }

    public void setOnOverflow(Runnable onOverflow) {
        this.onOverflow = onOverflow;
    }
}
