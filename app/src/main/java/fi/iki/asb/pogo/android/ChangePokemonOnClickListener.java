package fi.iki.asb.pogo.android;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import fi.iki.asb.pogo.pokemon.Pokemon;

public class ChangePokemonOnClickListener implements View.OnClickListener {

    private final Context context;
    private final Pokemon pokemon;

    public ChangePokemonOnClickListener(Context context, Pokemon pokemon) {
        this.context = context;
        this.pokemon = pokemon;
    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent(context, PokemonActivity.class);
        i.putExtra("POKEMON", PokemonSerializer.write(pokemon));
        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(i);
    }
}
