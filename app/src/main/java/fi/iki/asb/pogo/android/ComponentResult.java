package fi.iki.asb.pogo.android;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import fi.iki.asb.pogo.pokemon.Pokemon;

public class ComponentResult extends LinearLayout {

    private static final String[] CIRCLED = new String[]{
            "\u24ea",
            "\u2460",
            "\u2461",
            "\u2462",
            "\u2463",
            "\u2464",
            "\u2465",
            "\u2466",
            "\u2467",
            "\u2468",
            "\u2469",
            "\u246a",
            "\u246b",
            "\u246c",
            "\u246d",
            "\u246e",
    };

    private static int longClickHelpCount = 0;

    public ComponentResult(
            final Context context,
            final AttributeSet attrs,
            final Pokemon pokemon) {
        super(context, attrs);
        inflate(context, R.layout.component_result, this);

        final String ads = String.format("%s%s%s",
                CIRCLED[pokemon.getIndividualValues().getAttack()],
                CIRCLED[pokemon.getIndividualValues().getDefense()],
                CIRCLED[pokemon.getIndividualValues().getStamina()]);

        final String label = String.format(Locale.US, "%s / L%s / %.1f%%",
                ads,
                pokemon.getLevel().getName(),
                pokemon.getIndividualValues().getPerfection());

        final TextView tv = findViewById(R.id.textViewContent);
        tv.setText(label);
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
                StringBuilder toast = new StringBuilder();
                if (clipboard != null) {
                    clipboard.setPrimaryClip(ClipData.newPlainText(ads, ads));
                    toast.append(ads);
                    toast.append(" copied to clipboard.");
                } else {
                    toast.append("Clipboard is not available.");
                }

                if (longClickHelpCount < 3) {
                    longClickHelpCount++;
                    toast.append("\nLong click on a result to see evolution info.");
                }

                Toast.makeText(context, toast, Toast.LENGTH_SHORT).show();
            }
        });

        tv.setLongClickable(true);
        tv.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent i = new Intent(context, PokemonActivity.class);
                i.putExtra("POKEMON", PokemonSerializer.write(pokemon));
                context.startActivity(i);
                return true;
            }
        });
    }
}
