package fi.iki.asb.pogo.android;

import static fi.iki.asb.pogo.android.Const.*;

import android.app.Application;

import fi.iki.asb.pogo.pokedex.PokemonSpecies;
import fi.iki.asb.pogo.pokemon.Appraisal;
import fi.iki.asb.pogo.pokemon.Pokemon;

public class App extends Application {

    /**
     * Current pokemon.
     */
    private Pokemon pokemon;

    @Override
    public void onCreate() {
        super.onCreate();
        resetPokemon();
    }

    /**
     * Reset pokemon to default values.
     */
    public void resetPokemon() {
        if (pokemon != null) {
            setPokemon(Pokemon.ofSpecies(pokemon.getSpecies()));
        } else {
            setPokemon(Pokemon.ofSpecies(DEX.findByName("PIDGEY")));
        }
    }

    public Pokemon getPokemon() {
        return pokemon;
    }

    public int getMaxCp() {
        Pokemon maxPokemon = getMaxPokemon();
        if (CALC.calculateCp(maxPokemon)) {
            return maxPokemon.getCp();
        } else {
            return -1;
        }
    }

    public int getMaxHp() {
        Pokemon maxPokemon = getMaxPokemon();
        if (CALC.calculateHp(maxPokemon)) {
            return maxPokemon.getHp();
        } else {
            return -1;
        }
    }

    private Pokemon getMaxPokemon() {
        final Pokemon maxPokemon = Pokemon
                .from(pokemon);
        if (maxPokemon.getPowerUpCost() != null) {
            maxPokemon.setLevel(LEVELS.getMaxLevel(
                    maxPokemon.getPowerUpCost(),
                    maxPokemon.isPoweredUp(),
                    maxPokemon.isLucky()));
        } else {
            maxPokemon.setLevel(LEVELS.getLevel(40));
        }

        maxPokemon.getIndividualValues()
                .setSAD(15, 15, 15);
        return maxPokemon;
    }

    public void switchSpecies(PokemonSpecies species) {
        final Pokemon newPokemon = Pokemon.ofSpecies(species);
        newPokemon.setCp(pokemon.getCp());
        newPokemon.setHp(pokemon.getHp());
        newPokemon.setPowerUpCost(pokemon.getPowerUpCost());
        newPokemon.setPoweredUp(pokemon.isPoweredUp());

        final Appraisal appraisal = pokemon.getAppraisal();
        final Appraisal newAppraisal = newPokemon.getAppraisal();
        newAppraisal.setAttackBest(appraisal.isAttackBest());
        newAppraisal.setDefenseBest(appraisal.isDefenseBest());
        newAppraisal.setPerfectionTier(appraisal.getPerfectionTier());
        newAppraisal.setStaminaBest(appraisal.isStaminaBest());
        newAppraisal.setTopStatTier(appraisal.getTopStatTier());

        setPokemon(newPokemon);
    }

    private void setPokemon(Pokemon newPokemon) {
        pokemon = newPokemon;
    }
}
