package fi.iki.asb.pogo.android;

import static fi.iki.asb.pogo.android.Const.*;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import fi.iki.asb.pogo.pokemon.Pokemon;

public class PokemonActivity extends AppCompatActivity {

    private Pokemon pokemon = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        pokemon = PokemonSerializer.fromString(getIntent().getStringExtra("POKEMON"));

        setValues();
        addPredecessor();
        addSuccessors();
    }

    private void setValues() {
        ((TextView) findViewById(R.id.textViewPokemonSpecies)).setText(pokemon.getSpecies().getName());
        ((TextView) findViewById(R.id.textViewCP)).setText(pokemon.getCp().toString());
        ((TextView) findViewById(R.id.textViewHP)).setText(pokemon.getHp().toString());
        ((TextView) findViewById(R.id.textViewPerf)).setText(makePct(pokemon.getIndividualValues().getPerfection()));
        ((TextView) findViewById(R.id.textViewAtt)).setText(pokemon.getIndividualValues().getAttack().toString());
        ((TextView) findViewById(R.id.textViewDef)).setText(pokemon.getIndividualValues().getDefense().toString());
        ((TextView) findViewById(R.id.textViewSta)).setText(pokemon.getIndividualValues().getStamina().toString());
        ((TextView) findViewById(R.id.textViewLevel)).setText(pokemon.getLevel().getName());
    }

    private void addPredecessor() {
        String species = pokemon.getSpecies().getPredecessor();

        if (species == null) {
            findViewById(R.id.tableRowEvolvesFromTitle).setVisibility(View.GONE);
            findViewById(R.id.tableRowEvolvesFromContent).setVisibility(View.GONE);
        } else {
            findViewById(R.id.tableRowEvolvesFromTitle).setVisibility(View.VISIBLE);
            findViewById(R.id.tableRowEvolvesFromContent).setVisibility(View.VISIBLE);

            final Pokemon pred = pokemon.evolve(DEX.findByName(species));
            CALC.calculateHp(pred);
            CALC.calculateCp(pred);

            final TextView tv = findViewById(R.id.textViewEvolvesFromPokemon);
            tv.setText(makeText(pred));
            tv.setOnClickListener(new ChangePokemonOnClickListener(getBaseContext(), pred));
        }
    }

    private void addSuccessors() {
        final LinearLayout ll = findViewById(R.id.linearLayoutEvolvesToContent);
        ll.removeAllViews();

        final List<String> evolutions = pokemon.getSpecies().getEvolutions();
        if (evolutions.isEmpty()) {
            findViewById(R.id.tableRowEvolvesToTitle).setVisibility(View.GONE);
            return;
        } else {
            findViewById(R.id.tableRowEvolvesToTitle).setVisibility(View.VISIBLE);
        }

        for (String species: evolutions) {
            final Pokemon succ = pokemon.evolve(DEX.findByName(species));
            CALC.calculateHp(succ);
            CALC.calculateCp(succ);

            ComponentEvolution evo = new ComponentEvolution(getBaseContext(), null, succ);
            ll.addView(evo);
        }
    }

    private String makeText(Pokemon p) {
        return String.format(Locale.ENGLISH,
                "%s (%d)",
                p.getSpecies().getName(), p.getCp());
    }

    private String makePct(Double d) {
        if (d == null) {
            return "";
        }

        return String.format(Locale.US, "%.1f%%", d);
    }
}
