package fi.iki.asb.pogo.android;

import static fi.iki.asb.pogo.android.Const.*;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fi.iki.asb.pogo.calculator.Level;
import fi.iki.asb.pogo.pokedex.PokemonSpecies;
import fi.iki.asb.pogo.pokemon.Appraisal;
import fi.iki.asb.pogo.pokemon.Lucky;
import fi.iki.asb.pogo.pokemon.Pokemon;
import fi.iki.asb.pogo.pokemon.PoweredUp;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MiniIVCalculator";

    /**
     * See <code>hideKeyboardOnSpinnerEvent</code>.
     */
    private LinearLayout focusOnNothingKludge;

    private AutoCompleteTextView autoCompletePokemonSpecies;
    private EditText editTextCP;
    private EditText editTextHP;
    private CustomSpinner spinnerPerfection;
    private CustomSpinner spinnerTopStat;
    private CustomSpinner spinnerPowerUpCost;
    private CustomSpinner spinnerPoweredUp;
    private CustomSpinner spinnerLucky;
    private CheckBox checkBoxSta;
    private CheckBox checkBoxAtt;
    private CheckBox checkBoxDef;

    private TextChangeNumberOverflowListener cpChangeListener;
    private TextChangeNumberOverflowListener hpChangeListener;

    private boolean calculationDisabled = false;

    private final List<PokemonSpecies> pokemonSpecies = new ArrayList<>();
    private final List<Appraisal.PerfectionTier> perfectionChoices = Arrays.asList(Appraisal.PerfectionTier.values());
    private final List<Appraisal.TopStatTier> statChoices = Arrays.asList(Appraisal.TopStatTier.values());
    private final List<PoweredUp> poweredUp = Arrays.asList(PoweredUp.values());
    private final List<Lucky> lucky = Arrays.asList(Lucky.values());
    private final List<String> powerUpCosts = new ArrayList<>();
    private final List<String> powerUpCostsLucky = new ArrayList<>();

    private PokemonCollectorImpl collector = new PokemonCollectorImpl();
    private PokemonCollectorListenerGroup collectorListenerGroup = new PokemonCollectorListenerGroup();

    /**
     * There is a fucking bug in Android. For no bloody obvious reason AutoCompleteTextView
     * demands and gets focus every time we move focus to a component that is not an EditText.
     * Because we want, when focus is actually taken to AutoCompleteTextView by the user, the
     * text to be selected, we use AutoCompleteTextView.setSelectAllOnFocus. Because it's
     * obsession with having the focus, we end up the text in AutoCompleteTextView being selected
     * all the time and when the user takes the focus there, the selection is removed... and
     * the user can not easily erase the field, which is the use case we see most often.
     *
     * So to fix this bullshit we manually register listeners that set the focus on a dummy 0x0
     * pixel view, hide the soft keyboard and set selection in AutoCompleteTextView to none
     * every time a spinner is opened or closed or focus is moved to a non-EditText component.
     */
    private final CustomSpinner.OnSpinnerEventsListener hideKeyboardOnSpinnerEvent = new CustomSpinner.OnSpinnerEventsListener() {
        @Override
        public void onSpinnerOpened(Spinner spinner) {
            hideKeyboardAndFocusOnNothing();
        }

        @Override
        public void onSpinnerClosed(Spinner spinner) {
            hideKeyboardAndFocusOnNothing();
        }
    };

    private final View.OnFocusChangeListener hideKeyboardOnFocusGain = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (hasFocus) {
                hideKeyboardAndFocusOnNothing();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        collector.setListener(collectorListenerGroup);

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        setContentView(R.layout.activity_main);

        final ComponentResults results = findViewById(R.id.results);
        focusOnNothingKludge = findViewById(R.id.focusOnNothingKludge);
        autoCompletePokemonSpecies = findViewById(R.id.pokemonAutoComplete);
        editTextCP = findViewById(R.id.editTextCP);
        editTextHP = findViewById(R.id.editTextHP);
        spinnerPerfection = findViewById(R.id.spinnerPerfection);
        spinnerTopStat = findViewById(R.id.spinnerBestStat);
        spinnerPowerUpCost = findViewById(R.id.spinnerPowerUpCost);
        spinnerPoweredUp = findViewById(R.id.spinnerPoweredUp);
        spinnerLucky = findViewById(R.id.spinnerLucky);
        checkBoxSta = findViewById(R.id.checkBoxSta);
        checkBoxAtt = findViewById(R.id.checkBoxAtt);
        checkBoxDef = findViewById(R.id.checkBoxDef);

        powerUpCosts.add("Unknown");
        powerUpCostsLucky.add("Unknown");
        for (Level level : LEVELS.getAllLevels()) {
            String s = String.valueOf(level.getPowerUpCost(Lucky.NO));
            if (!powerUpCosts.contains(s)) {
                powerUpCosts.add(s);
            }

            s = String.valueOf(level.getPowerUpCost(Lucky.YES));
            if (!powerUpCostsLucky.contains(s)) {
                powerUpCostsLucky.add(s);
            }
        }

        collectorListenerGroup.add(results);

        initComponents();
        primeComponents();
        calculate();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Never show keyboard unless necessary.
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        hideKeyboardAndFocusOnNothing();
    }

    private void initComponents() {
        calculationDisabled = true;

        initAutoCompletePokemonSpecies();
        initEditTextCP();
        initEditTextHP();
        initSpinnerPerfection();
        initSpinnerTopStat();
        initSpinnerPowerUpCost();
        initSpinnerPoweredUp();
        initSpinnerLucky();
        initCheckBoxes();

        calculationDisabled = false;
    }

    private void primeComponents() {
        calculationDisabled = true;

        primeAutoCompletePokemonSpecies();
        primeEditTextCP();
        primeEditTextHP();
        primeSpinnerPerfection();
        primeSpinnerTopStat();
        primeSpinnerPowerUpCost();
        primeSpinnerPoweredUp();
        primeSpinnerLucky();
        primeCheckBoxes();

        calculationDisabled = false;
    }

    public void clearValues(View component) {
        app().resetPokemon();
        primeComponents();

        calculate();

        editTextCP.requestFocus();

        // Force soft keyboard to show up.
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }
    }

    private void initAutoCompletePokemonSpecies() {
        pokemonSpecies.clear();
        pokemonSpecies.addAll(DEX.find(AllPokemonPredicate.instance()));

        ArrayAdapter<PokemonSpecies> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, pokemonSpecies);
        autoCompletePokemonSpecies.setAdapter(adapter);
        autoCompletePokemonSpecies.setSelectAllOnFocus(true);
        autoCompletePokemonSpecies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switchSpecies((PokemonSpecies) parent.getItemAtPosition(position));
                editTextCP.requestFocus();
            }
        });
    }

    private void primeAutoCompletePokemonSpecies() {
        autoCompletePokemonSpecies.setText(pok().getSpecies().getName());
    }

    private void initEditTextCP() {
        editTextCP.setSelectAllOnFocus(true);

        cpChangeListener = new TextChangeNumberOverflowListener();
        cpChangeListener.setOnNextDigitOverflows(new Runnable() {
            @Override
            public void run() {
                editTextHP.requestFocus();
            }
        });

        cpChangeListener.setOnOverflow(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, getString(R.string.maxCpExceeded,
                        pok().getSpecies().getName(),
                        app().getMaxCp()), Toast.LENGTH_SHORT).show();
            }
        });

        cpChangeListener.setOnValueChanged(new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) {
                pok().setCp(integer);
                calculate();
            }
        });

        editTextCP.addTextChangedListener(cpChangeListener);
    }

    private void primeEditTextCP() {
        editTextCP.setText(pok().getCp() != null ? pok().getCp().toString() : "");
    }

    private void initEditTextHP() {
        editTextHP.setSelectAllOnFocus(true);

        hpChangeListener = new TextChangeNumberOverflowListener();
        hpChangeListener.setOnOverflow(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, getString(R.string.maxHpExceeded,
                        pok().getSpecies().getName(),
                        app().getMaxHp()), Toast.LENGTH_SHORT).show();
            }
        });

        hpChangeListener.setOnValueChanged(new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) {
                pok().setHp(integer);
                calculate();
            }
        });

        editTextHP.addTextChangedListener(hpChangeListener);
    }

    private void primeEditTextHP() {
        editTextHP.setText(pok().getHp() != null ? pok().getHp().toString() : "");
    }

    private void initSpinnerPerfection() {
        final ArrayAdapter<Appraisal.PerfectionTier> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, perfectionChoices);
        spinnerPerfection.setAdapter(adapter);
        spinnerPerfection.setOnFocusChangeListener(hideKeyboardOnFocusGain);
        spinnerPerfection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // NOP
            }

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final Object item = parent.getItemAtPosition(position);
                pok().getAppraisal().setPerfectionTier((Appraisal.PerfectionTier) item);

                calculate();
            }
        });

        spinnerPerfection.setSpinnerEventsListener(hideKeyboardOnSpinnerEvent);
    }

    private void primeSpinnerPerfection() {
        spinnerPerfection.setSelection(perfectionChoices.indexOf(pok().getAppraisal().getPerfectionTier()));
    }

    private void initSpinnerTopStat() {
        final ArrayAdapter<Appraisal.TopStatTier> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, statChoices);
        spinnerTopStat.setAdapter(adapter);
        spinnerTopStat.setOnFocusChangeListener(hideKeyboardOnFocusGain);
        spinnerTopStat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // NOP
            }

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final Object item = parent.getItemAtPosition(position);
                pok().getAppraisal().setTopStatTier((Appraisal.TopStatTier) item);

                calculate();
            }
        });

        spinnerTopStat.setSpinnerEventsListener(hideKeyboardOnSpinnerEvent);
    }

    private void primeSpinnerTopStat() {
        spinnerTopStat.setSelection(statChoices.indexOf(pok().getAppraisal().getTopStatTier()));
    }

    private void initSpinnerPowerUpCost() {
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line,
                pok().isLucky().isLucky() ? powerUpCostsLucky : powerUpCosts);
        spinnerPowerUpCost.setAdapter(adapter);
        spinnerPowerUpCost.setOnFocusChangeListener(hideKeyboardOnFocusGain);
        spinnerPowerUpCost.setOnItemSelectedListener(new ItemSelectionListener() {
            @Override
            public void onItemSelected(Object item) {
                try {
                    if ("Unknown".equals(item.toString())) {
                        pok().setPowerUpCost(null);
                    } else {
                        pok().setPowerUpCost(Integer.parseInt(item.toString()));
                    }
                } catch (NumberFormatException ex) {
                    // NOP.
                }

                calculate();
            }
        });

        spinnerPowerUpCost.setSpinnerEventsListener(hideKeyboardOnSpinnerEvent);
    }

    private void primeSpinnerPowerUpCost() {
        String str = "Unknown";
        if (pok().getPowerUpCost() != null) {
            str = pok().getPowerUpCost().toString();
        }
        spinnerPowerUpCost.setSelection(powerUpCosts.indexOf(str));
    }

    private void initSpinnerPoweredUp() {
        final ArrayAdapter<PoweredUp> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, poweredUp);
        spinnerPoweredUp.setAdapter(adapter);
        spinnerPoweredUp.setOnFocusChangeListener(hideKeyboardOnFocusGain);
        spinnerPoweredUp.setOnItemSelectedListener(new ItemSelectionListener() {
            @Override
            public void onItemSelected(Object item) {
                pok().setPoweredUp((PoweredUp) item);
                calculate();
            }
        });

        spinnerPoweredUp.setSpinnerEventsListener(hideKeyboardOnSpinnerEvent);
    }

    private void primeSpinnerPoweredUp() {
        spinnerPoweredUp.setSelection(poweredUp.indexOf(pok().isPoweredUp()));
    }

    private void initSpinnerLucky() {
        final ArrayAdapter<Lucky> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, lucky);
        spinnerLucky.setAdapter(adapter);
        spinnerLucky.setOnFocusChangeListener(hideKeyboardOnFocusGain);
        spinnerLucky.setOnItemSelectedListener(new ItemSelectionListener() {
            @Override
            public void onItemSelected(Object item) {
                pok().setLucky((Lucky) item);

                int pos = spinnerPowerUpCost.getSelectedItemPosition();
                initSpinnerPowerUpCost();
                spinnerPowerUpCost.setSelection(pos);

                calculate();
            }
        });

        spinnerLucky.setSpinnerEventsListener(hideKeyboardOnSpinnerEvent);
    }

    private void primeSpinnerLucky() {
        spinnerLucky.setSelection(lucky.indexOf(pok().isLucky()));
    }

    private void initCheckBoxes() {
        CompoundButton.OnCheckedChangeListener listener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView == checkBoxSta) {
                    pok().getAppraisal().setStaminaBest(isChecked);
                }
                if (buttonView == checkBoxAtt) {
                    pok().getAppraisal().setAttackBest(isChecked);
                }
                if (buttonView == checkBoxDef) {
                    pok().getAppraisal().setDefenseBest(isChecked);
                }

                hideKeyboardAndFocusOnNothing();
                calculate();
            }
        };

        checkBoxSta.setOnCheckedChangeListener(listener);
        checkBoxSta.setOnFocusChangeListener(hideKeyboardOnFocusGain);
        checkBoxAtt.setOnCheckedChangeListener(listener);
        checkBoxAtt.setOnFocusChangeListener(hideKeyboardOnFocusGain);
        checkBoxDef.setOnCheckedChangeListener(listener);
        checkBoxDef.setOnFocusChangeListener(hideKeyboardOnFocusGain);
    }

    private void primeCheckBoxes() {
        checkBoxSta.setChecked(pok().getAppraisal().isStaminaBest());
        checkBoxAtt.setChecked(pok().getAppraisal().isAttackBest());
        checkBoxDef.setChecked(pok().getAppraisal().isDefenseBest());
    }

    private void switchSpecies(PokemonSpecies newSpecies) {
        app().switchSpecies(newSpecies);
        autoCompletePokemonSpecies.setText(newSpecies.getName());
        calculate();
    }

    private void calculate() {
        if (calculationDisabled)
            return;

        CALC.calculateIV(pok(), collector);

        cpChangeListener.setMaxValue(app().getMaxCp());
        hpChangeListener.setMaxValue(app().getMaxHp());
    }

    private App app() {
        return (App) getApplication();
    }

    private Pokemon pok() {
        return app().getPokemon();
    }

    private void hideKeyboardAndFocusOnNothing() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (imm == null) {
            return;
        }

        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            Log.d(TAG, "Kludge to get window token");
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        focusOnNothingKludge.requestFocus();
        autoCompletePokemonSpecies.setSelection(0, 0);
    }
}
