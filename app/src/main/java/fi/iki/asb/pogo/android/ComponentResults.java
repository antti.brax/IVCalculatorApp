package fi.iki.asb.pogo.android;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import fi.iki.asb.pogo.pokemon.Pokemon;

public class ComponentResults
        extends LinearLayout
        implements PokemonCollectorListener {

    private final Context context;
    private final AttributeSet attrs;
    private final TextView textViewNoResults;
    private final TextView textViewTooManyResults;
    private final LinearLayout linearLayout;

    public ComponentResults(
            final Context context,
            final AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.component_results, this);

        this.context = context;
        this.attrs = attrs;
        textViewNoResults = findViewById(R.id.textViewNoResults);
        textViewTooManyResults = findViewById(R.id.textViewTooManyResults);
        linearLayout = findViewById(R.id.linearLayout);
    }

    @Override
    public void onCollectingEnded(List<Pokemon> results, boolean interrupted) {
        linearLayout.removeAllViews();

        if (results.size() == 0) {
            textViewNoResults.setVisibility(VISIBLE);
            textViewTooManyResults.setVisibility(GONE);
            return;
        } else {
            textViewNoResults.setVisibility(GONE);
        }

        if (interrupted) {
            textViewTooManyResults.setVisibility(VISIBLE);
        } else {
            textViewTooManyResults.setVisibility(GONE);
        }

        for (Pokemon pokemon : results) {
            linearLayout.addView(new ComponentResult(context, attrs, pokemon));
        }
    }
}