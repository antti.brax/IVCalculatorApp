package fi.iki.asb.pogo.android;

import fi.iki.asb.pogo.pokedex.PokemonSpecies;
import fi.iki.asb.pogo.util.Predicate;

/**
 * Predicate that finds all pokemon (from generations 1, 2, 3 and 4).
 */
public class AllPokemonPredicate implements Predicate<PokemonSpecies> {

    private static final AllPokemonPredicate INSTANCE = new AllPokemonPredicate();

    private AllPokemonPredicate() {
        // NOP
    }

    public static AllPokemonPredicate instance() {
        return INSTANCE;
    }

    @Override
    public boolean test(PokemonSpecies pokemonSpecies) {
        return true;
    }
}
